# The name $name should match a wallet file object of the form
# ssh-rsa/gitlab-$name.
#
# Example:
#   gitlab_deploykey { 'adamskey':
#   }
#
# will download the SSH private key wallet object 'ssh-rsa/gitlab-adamskey' into
# /root/.ssh/id_rsa and the SSH public key stored in the files/

define gitlab_deploykey (
  $key_type = 'rsa',
  $ensure   = 'present',
) {

  include wallet::client

  # Get the code.stanford.edu (GitLab) private key from wallet and put it
  # in the ssh config directory.
  wallet { "ssh-${key_type}/gitlab-${name}":
    ensure => $ensure,
    type   => 'file',
    path   => '/root/.ssh/id_rsa',
    mode   => '0600',
  }


}
